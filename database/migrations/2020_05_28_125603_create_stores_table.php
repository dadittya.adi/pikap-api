<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stores', function (Blueprint $table) {
            $table->id();
            $table->string('name', 191);
            $table->string('email', 191);
            $table->string('avatar', 255)->nullable();
            $table->string('banner', 255)->nullable();
            $table->string('facebook_url', 191)->nullable();
            $table->string('instagram_url', 191)->nullable();
            $table->string('twitter_url', 191)->nullable();
            $table->text('address');
            $table->string('phone_number', 20);
            $table->decimal('longitude', 9, 6)->nullable();
            $table->decimal('latitude', 9, 6)->nullable();
            $table->foreignId('province_id')->nullable()->constrained('provinces');
            $table->foreignId('city_id')->nullable()->constrained('cities');
            $table->foreignId('district_id')->nullable()->constrained('districts');
            $table->foreignId('village_id')->nullable()->constrained('villages');
            $table->string('zip_code', 30)->nullable();
            $table->boolean('is_active')->default(0);
            $table->foreignId('merchant_id')->constrained('merchants');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stores');
    }
}
