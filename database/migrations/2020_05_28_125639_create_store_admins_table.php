<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStoreAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('store_admins', function (Blueprint $table) {
            $table->id();
            $table->foreignId('store_id')->constrained('stores');
            $table->foreignId('user_level_id')->constrained('user_levels');
            $table->string('email', 191)->unique();
            $table->string('password', 191);
            $table->string('name', 100);
            $table->string('phone_number', 20);
            $table->string('token', 230)->nullable();
            $table->string('verification_code', 191)->nullable();
            $table->boolean('is_active')->default(1);
            $table->timestamp('last_active')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store_admins');
    }
}
