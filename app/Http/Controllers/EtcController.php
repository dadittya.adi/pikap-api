<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use stdClass;
use Validator;

class EtcController extends Controller
{

	public function checkEmail(Request $request, $type = null)
	{
		$response = new stdClass();
		$field = $this->checkField();
		$validation = [];
		if ($field == 'email') {
			$validation['identity'] = 'required|email|unique:users,email';
			if ($type == 'store') {
				$validation['identity'] = 'required|email|unique:store_admins,email';
			}
		} else {
			$validation['identity'] = 'required|unique:users,phone_number';
			if ($type == 'store') {
				$validation['identity'] = 'required|unique:store_admins,phone_number';
			}
		}
		$validator = Validator::make($request->all(), $validation);

		if ($validator->fails()) {
			$response->code = 201;
			$response->message = 'Email / phone number has already been taken.';
			return response()->json(['error' => $response], 201);
		}

		$response->code = 200;
		$response->message = 'success';
		return response()->json($response, 200);
	}

	public function checkField()
	{
		$login = request()->input('identity');

		$field = filter_var($login, FILTER_VALIDATE_EMAIL) ? 'email' : 'phone_number';
		request()->merge([$field => $login]);

		return $field;
	}

	public function checkStoreName(Request $request)
	{
		$response = new stdClass();
		$validator = Validator::make($request->all(), [
			'store_name' => 'required|unique:stores,name',
		]);

		if ($validator->fails()) {
			$response->code = 201;
			$response->message = 'Store name has already been taken.';
			return response()->json(['error' => $response], 201);
		}

		$response->code = 200;
		$response->message = 'success';
		return response()->json($response, 200);
	}
}
