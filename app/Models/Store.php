<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Store extends Model
{
  //
  protected $table = 'stores';
  protected $fillable = [
    'name',
    'email',
    'merchant_id',
    'avatar',
    'banner',
    'facebook_url',
    'twitter_url',
    'instagram_url',
    'address',
    'phone_number',
    'longitude',
    'latitude',
    'province_id',
    'city_id',
    'district_id',
    'village_id',
    'zip_code',
    'is_active',
    'order_processing_time',
    'description',
    'area_id'
  ];

  protected $appends = ['following', 'follower_count', 'follower_count_str'];

  public function merchant()
  {
    return $this->belongsTo('App\Models\Merchant', 'merchant_id');
  }

  public function province()
  {
    return $this->belongsTo('App\Models\Province', 'province_id');
  }

  public function city()
  {
    return $this->belongsTo('App\Models\City', 'City_id');
  }

  public function district()
  {
    return $this->belongsTo('App\Models\District', 'district_id');
  }

  public function village()
  {
    return $this->belongsTo('App\Models\Village', 'village_id');
  }

  public function storeAdmins()
  {
    return $this->hasMany('App\Models\StoreAdmin', 'store_id');
  }

  public function products()
  {
    return $this->hasMany('App\Models\Product', 'store_id');
  }

  public function usersFollowing()
  {
    return $this->belongsToMany('App\Models\User', 'follows', 'store_id', 'user_id');
  }

  public function transactionDetails()
  {
    return $this->hasMany('App\Models\TransactionDetail', 'store_id');
  }

  public function getAvatarImageAttribute()
  {
    $image = url('/') . '/images/default.jpg';
    if ($this->avatar)
      $image = route('media.product', ['filename' => $this->avatar]);

    return $image;
  }

  public function getBannerImageAttribute()
  {
    $image = url('/') . '/images/default.jpg';
    if ($this->banner)
      $image = route('media.product', ['filename' => $this->banner]);

    return $image;
  }

  public function getFollowingAttribute()
  {
    $auth = Auth::user();
    $res = false;
    if ($auth)
      $res = $this->usersFollowing()->find($auth->id);
    return $res ? true : false;
  }

  public function getFollowerCountAttribute()
  {
    return $this->usersFollowing()->count();
  }

  public function getFollowerCountStrAttribute()
  {
    $count = scaleNumber($this->usersFollowing()->count());
    return $count;
  }
}
