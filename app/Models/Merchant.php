<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Merchant extends Model
{
  //
  protected $table = 'merchants';
  protected $fillable = [
    'name',
    'email',
    'password',
    'is_active',
    'verification_code',
  ];

  protected $hidden = [
    'password',
    'verification_code'
  ];

  public function stores()
  {
    return $this->hasMany('App\Models\Store', 'merchant_id');
  }
}
