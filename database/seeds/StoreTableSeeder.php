<?php

use Illuminate\Database\Seeder;

use App\Models\Merchant;
use App\Models\Store;
use App\Models\StoreAdmin;
use App\Models\UserLevel;
use Illuminate\Support\Facades\Hash;

class StoreTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $name = 'store example1';
    $email = 'store1@example.com';
    $password = Hash::make('password');
    $phone_number = 87912;
    $address = 'asdfsdf';

    $merchant = new Merchant();
    $merchant->fill([
      'name' => $name,
      'email' => $email,
      'password' => $password,
      'is_active' => 1,
    ]);
    $merchant->save();

    $store = new Store();
    $store->fill([
      'name' => $name,
      'email' => $email,
      'merchant_id' => $merchant->id,
      'phone_number' => $phone_number,
      'address' => $address,
      'is_active' => 1,
    ]);
    $store->save();

    $userLevel = UserLevel::where('name', 'SUPERADMIN')->first();
    $storeAdmin = new StoreAdmin();
    $storeAdmin->fill([
      'store_id' => $store->id,
      'user_level_id' => $userLevel->id,
      'email' => $email,
      'password' => $password,
      'phone_number' => $phone_number,
      'name' => $name,
      'is_actiove' => 1
    ]);
  }
}
