<?php

use App\Models\StoreAdmin;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

if (!function_exists('isStoreSuperAdmin')) {
  function isStoreSuperAdmin()
  {
    $result = false;
    $auth = Auth::user();
    $admin = StoreAdmin::where('email', $auth->email)->first();
    if ($admin && $admin->userLevel->name == 'SUPERADMIN') {
      $result = true;
    }
    return $result;
  }
}

if (!function_exists('isGuest')) {
  function isGuest($request)
  {
    $result = false;
    $auth = Auth::user();
    if ($auth && $request->userType == 'guest') {
      $result = true;
    }
    return $result;
  }
}

if (!function_exists('isMerchant')) {
  function isMerchant($request)
  {
    $result = false;
    $auth = Auth::user();
    if ($auth && $request->userType == 'store_admin') {
      $result = true;
    }
    return $result;
  }
}

if (!function_exists('generateRandomString')) {
  function generateRandomString($length = 128, $number = true, $alphabet = true)
  {
    $char = '';
    if ($number)
      $char .= '0123456789';
    if ($alphabet)
      $char .= 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charLength = strlen($char);
    $str = '';
    for ($i = 0; $i < $length; $i++) {
      $str .= $char[rand(0, $charLength - 1)];
    }
    return $str;
  }
}

if (!function_exists('scaleNuber')) {
  function scaleNumber($num)
  {
    $num = intval($num);

    if ($num < 1000) return strval($num);
    $k = 1000;
    $decimals = 2;
    // source formating https://minershaven.fandom.com/wiki/Cash_Suffixes
    $sizes = [
      '',
      'K',
      'M',
      'B',
      'T',
    ];
    $i = floor(log(abs($num)) / log($k));
    return number_format(floatval(($num / pow($k, $i))), $decimals) . '' . $sizes[$i];
  }
}
