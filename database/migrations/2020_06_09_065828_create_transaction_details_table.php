<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_details', function (Blueprint $table) {
            $table->id();
            $table->foreignId('transaction_id')->constrained('transactions');
            $table->foreignId('store_id')->constrained('stores');
            $table->string('transaction_number', 40)->unique();
            $table->boolean('is_paid')->default(0);
            $table->string('pickup_code', 40)->nullable();
            $table->unsignedInteger('rating')->nullable();
            $table->string('status', 30);
            $table->decimal('subtotal', 19, 2)->default(0);
            $table->string('shipping_type', 50);
            $table->decimal('shipping_price', 19, 2)->default(0);
            $table->decimal('total_amount', 19, 2)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_details');
    }
}
