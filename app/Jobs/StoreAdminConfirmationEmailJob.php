<?php

namespace App\Jobs;

use App\Models\StoreAdmin;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Queue\SerializesModels;

class StoreAdminConfirmationEmailJob extends Job
{
  use SerializesModels;
  protected $user;
  protected $verification_code_unhash;
  public function __construct(StoreAdmin $user, $verification_code)
  {
    $this->verification_code_unhash = $verification_code;
    $this->user = $user;
  }

  public function handle(Mailer $mailer)
  {
    $mailer->send(
      'account_confirmation',
      [
        'customer' => $this->user,
        'verification_code' => $this->verification_code_unhash
      ],
      function ($message) {
        $message->to($this->user->email, $this->user->name)->subject("Confirmation Account");
      }
    );
  }
}
