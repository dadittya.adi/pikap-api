<?php

use Illuminate\Database\Seeder;

use Carbon\Carbon;
use App\Models\Admin;
use Illuminate\Support\Facades\Hash;

class AdminTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $admin                       = new Admin();
    $admin->name                 = 'admin';
    $admin->email                = 'admin@example.com';
    $admin->email_verified_at    = carbon::now();
    $admin->password             = Hash::make('password');
    $admin->save();
  }
}
