<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserLevel extends Model
{
  //
  protected $table = 'user_levels';
  protected $fillable = ['name'];

  public function storeAdmins()
  {
    return $this->hasMany('App\Models\StoreAdmin', 'user_level_id');
  }
}
