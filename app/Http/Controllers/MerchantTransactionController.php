<?php

namespace App\Http\Controllers;

use App\Models\Store;
use App\Models\StoreAdmin;
use App\Models\TransactionDetail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use stdClass;
use Validator;
use Illuminate\Support\Facades\DB;

class MerchantTransactionController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}

	public function lists(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'status' => 'sometimes|in:NEW,PROCESS,READY,FINISH',
		]);

		if ($validator->fails()) {
			$results = new StdClass();
			$results->code = 203;
			$results->error = $validator->errors();
			return response()->json(['error' => $results], 203);
		}

		$count = $request->count ? $request->count : 10;
		$page = $request->page ? $request->page : 1;
		$skip = (intval($page) - 1) * $count;
		$response = new stdClass();
		setlocale(LC_TIME, 'Indonesia');
		Carbon::setLocale('id');

		if (isMerchant($request)) {
			$auth = Auth::user();
			$admin = StoreAdmin::find($auth->id);
			$store = $admin->store;
			$transactionLists = $store
				->transactionDetails();
			if ($request->status) {
				$transactionLists = $transactionLists->where('transaction_details.status', $request->status);
			}
			$transactionLists = $transactionLists->offset($skip)
				->limit($count)
				->orderBy('created_at', 'DESC')
				->get();

			if (!count($transactionLists)) {
				$response = new StdClass();
				$response->code = 204;
				$response->message = 'No data found';

				return response()->json(['error' => $response], 200);
			}

			foreach ($transactionLists as $key => $list) {
				# code...
				$list->date = Carbon::parse($list->transaction()->first()->date)
					->formatLocalized('%d %B %Y %H:%M:00');
			}

			$response->code = 200;
			$response->results = $transactionLists;

			return response()->json($response, 200);
		}

		$response->code = 401;
		$response->message = 'Unauthorized';

		return response()->json(['error' => $response], 401);
	}

	public function details(Request $request, $id)
	{
		$response = new stdClass();
		$transactionDetail = TransactionDetail::find($id);

		if (!$transactionDetail) {
			$response = new StdClass();
			$response->code = 204;
			$response->message = 'No data found';

			return response()->json(['error' => $response], 200);
		}

		$auth = Auth::user();

		if (isMerchant($request) && $auth->store_id == $transactionDetail->store_id) {
			$transaction = $transactionDetail->transaction;
			$customer = $transaction
				->with('user:id,name,email,phone_number as phone')->get();
			$transactionDetail->payment_type = $transaction->payment_type;
			$transactionDetail->payment_status = $transaction->payment_status;
			$transactionDetail->user = $customer[0]->user;
			$items = [];
			foreach ($transactionDetail->items as $key => $value) {
				# code...
				$item = new stdClass();
				$item->id = $value->id;
				$item->product_name = $value->name;
				$item->sku = $value->sku;
				$item->weight = $value->weight;
				$item->quantity = $value->pivot->quantity;
				$item->price = $value->pivot->price;
				$item->total_amount = $value->pivot->total_amount;
				$item->notes = $value->pivot->notes;
				$productImage = $value->productImages->first();

				if ($productImage)
					$item->image = route('media.product', ['filename' => $productImage->image]);
				else
					$item->image = url('/') . '/images/default.jpg';
				unset($item->pivot);
				$items[] = $item;
			}

			unset($transactionDetail->transaction);
			unset($transactionDetail->items);
			$transactionDetail->items = $items;
			$response->code = 200;
			$response->results = $transactionDetail;

			return response()->json($response, 200);
		}

		$response->code = 401;
		$response->message = 'Unauthorized';

		return response()->json(['error' => $response], 401);
	}

	public function detailUseCode(Request $request, $code)
	{
		$response = new stdClass();
		$transactionDetail = TransactionDetail::where('transaction_number', $code)->first();

		if (!$transactionDetail) {
			$response = new StdClass();
			$response->code = 204;
			$response->message = 'No data found';

			return response()->json(['error' => $response], 200);
		}

		$auth = Auth::user();

		if (isMerchant($request) && $auth->store_id == $transactionDetail->store_id) {
			$transaction = $transactionDetail->transaction;
			$customer = $transaction
				->with('user:id,name,email,phone_number as phone')->get();
			$transactionDetail->payment_type = $transaction->payment_type;
			$transactionDetail->payment_status = $transaction->payment_status;
			$transactionDetail->user = $customer[0]->user;
			$items = [];
			foreach ($transactionDetail->items as $key => $value) {
				# code...
				$item = new stdClass();
				$item->id = $value->id;
				$item->product_name = $value->name;
				$item->sku = $value->sku;
				$item->weight = $value->weight;
				$item->quantity = $value->pivot->quantity;
				$item->price = $value->pivot->price;
				$item->total_amount = $value->pivot->total_amount;
				$item->notes = $value->pivot->notes;
				$productImage = $value->productImages->first();

				if ($productImage)
					$item->image = route('media.product', ['filename' => $productImage->image]);
				else
					$item->image = url('/') . '/images/default.jpg';
				unset($item->pivot);
				$items[] = $item;
			}

			unset($transactionDetail->transaction);
			unset($transactionDetail->items);
			$transactionDetail->items = $items;
			$response->code = 200;
			$response->results = $transactionDetail;

			return response()->json($response, 200);
		}

		$response->code = 401;
		$response->message = 'Unauthorized';

		return response()->json(['error' => $response], 401);
	}

	public function update(Request $request, $id)
	{
		$validator = Validator::make($request->all(), [
			'status' => 'required|in:NEW,PROCESS,READY,FINISH',
		]);

		if ($validator->fails()) {
			$results = new StdClass();
			$results->code = 203;
			$results->error = $validator->errors();
			return response()->json(['error' => $results], 203);
		}

		$response = new stdClass();
		$transactionDetail = TransactionDetail::find($id);

		if (!$transactionDetail) {
			$response = new StdClass();
			$response->code = 204;
			$response->message = 'No data found';

			return response()->json(['error' => $response], 200);
		}

		$auth = Auth::user();

		if (isMerchant($request) && $auth->store_id == $transactionDetail->store_id) {
			$transactionDetail->status = $request->status;

			if ($request->status == 'READY' && $transactionDetail->shipping_type == 'COP') {
				$transactionDetail->pickup_code = generateRandomString(10, true, false);
			}

			$transactionDetail->save();

			$response->code = 200;
			$response->results = $transactionDetail;

			return response()->json($response, 200);
		}

		$response->code = 401;
		$response->message = 'Unauthorized';

		return response()->json(['error' => $response], 401);
	}

	public function statistics(Request $request)
	{

		$response = new stdClass();

		$auth = Auth::user();

		if (isMerchant($request)) {
			$store = Store::find($auth->store_id);
			$status = ['NEW', 'PROCESS', 'READY', 'FINISH'];
			$results = $store
				->transactionDetails()
				->select('status', DB::raw('count(*) as total'))
				->groupBy('status')
				->get();
			$statistics = [];
			foreach ($status as $value) {
				$filtered_collection = $results->filter(function ($item) use ($value) {
					return $item->status == $value;
				})->values();

				$statistic = new stdClass();
				$statistic->status = $value;
				$statistic->count = count($filtered_collection) ? $filtered_collection[0]->total : 0;
				$statistic->count_str = count($filtered_collection) ?
					scaleNumber($filtered_collection[0]->total) : "0";
				$statistics[] = $statistic;
			}
			$response->code = 200;
			$response->results = $statistics;
			return response()->json($response, 200);
		}

		$response->code = 401;
		$response->message = 'Unauthorized';

		return response()->json(['error' => $response], 401);
	}
}
