<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\District;
use App\Models\Province;
use stdClass;

class RegionController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    //
  }

  //
  public function getProvince($id = null)
  {
    $response = new stdClass();
    $response->status = 200;
    $response->message = 'success';

    $provinces = Province::get();
    if (!count($provinces)) {
      $response->status = 204;
      $response->message = "No data found";
      return response()->json($response);
    }

    $response->data = $provinces;
    return response()->json($response);
  }

  function getCity($province_id = null)
  {
    $province = Province::find($province_id);
    $response = new stdClass();
    $response->status = 200;
    $response->message = 'success';

    if (!$province) {
      $response->status = 204;
      $response->message = "No data found";
      return response()->json($response);
    }

    $cities = $province->cities;
    $response->data = $cities;
    return response()->json($response);
  }

  function getCityByStatus($status){
    $cities = new City();
    $response = new stdClass();

    if($status == 'active'){
      $cities = $cities->where('status', 1)->get();
    }else if($status == 'upcoming'){
      $cities = $cities->where('status', 2)->get();
    }else{
      $response->status = 204;
      $response->message = "No data found";
      return response()->json(['error' => $response], 200);
    }

    if(!count($cities)){
      $response->status = 204;
      $response->message = "No data found";
      return response()->json(['error' => $response], 200);
    }

    $response->data = $cities;
    return response()->json($response, 200);
  }

  public function getDistrict($city_id = null)
  {
    $city = City::find($city_id);
    $response = new stdClass();
    $response->status = 200;
    $response->message = 'success';

    if (!$city) {
      $response->status = 204;
      $response->message = "No data found";
      return response()->json($response);
    }

    $districts = $city->districts;
    $response->data = $districts;
    return response()->json($response);
  }

  public function getVillage($district_id)
  {
    $district = District::find($district_id);
    $response = new stdClass();
    $response->status = 200;
    $response->message = 'success';

    if (!$district) {
      $response->status = 204;
      $response->message = "No data found";
      return response()->json($response);
    }

    $villages = $district->villages;
    $response->data = $villages;
    return response()->json($response);
  }
}
