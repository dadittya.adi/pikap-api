<!DOCTYPE html>
<html lang="en-US">

<head>
  <meta charset="utf-8">
</head>

<body>
  <h3>Confirmation of your account with Max Virtual Race</h3>

  <div>
    <p style="margin: 1em 0;">Thank you for signing up with Max Virtual Race!<br>Please kindly confirm your account by clicking below URL and for further transaction:</p>
    <p style="margin: 1em 0 1em 15px;">
      {{ $verification_code }}
    </p>
    <p style="margin: 1em 0;">Thank you for choosing Max Virtual Race.</p>

    <p style="margin: 1em 0;">Cheers and Love<br>Max Virtual Race</p>

    <p style="margin: 1em 0;">If you have any inquiries, please kindly email us at info@maxvirtualrace.com</p>
    <!--
			<p style="margin: 1em 0;">Our working hours is :<br>
				Monday – Friday	: 08:00 – 16:00 WIB<br>
				Saturday		: 08:00 – 13:00 WIB
			</p>
			!-->
    <p style="margin: 1em 0;">We will reply you as soon as we could and please do not reply to this email.</p>
  </div>

  <div style="font-size: 11px; margin-top: 10px">
    This email was sent automatically by Max Virtual Race.
  </div>
  <br>
  <hr>
  <div>
    <p style="margin: 1em 0;">
      Terima kasih telah mendaftar di Max Virtual Race!<br>
      Mohon klik link di bawah ini untuk transaksi selanjutnya:
    </p>
    <p style="margin: 1em 0 1em 15px">
      {{ $customer->verification_code_unhash }}
    </p>
    <p style="margin: 1em 0;">
      Terima kasih telah memilih Max Virtual Race.<br>
      {{-- Selamat berbelanja! --}}
    </p>

    <p style="margin: 1em 0;">
      Salam Hangat<br>
      Max Virtual Race
    </p>

    <p style="margin: 1em 0;">Jika anda mempunyai pertanyaan atau keluhan, silahkan email kami di info@maxvirtualrace.com </p>
    <!--
			<p style="margin: 1em 0;">Our working hours is :<br>
				Senin – Jum'at	: 08:00 – 16:00 WIB<br>
				Sabtu		: 08:00 – 13:00 WIB
			</p>
			!-->
    <p style="margin: 1em 0;">Kami akan membalas segera dan harap jangan membalas email ini.</p>
  </div>

  <div style="font-size: 11px; margin-top: 10px">
    This email was sent automatically by Max Virtual Race.
  </div>

</body>

</html>