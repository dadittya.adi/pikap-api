<?php

namespace App\Http\Controllers;

use App\Models\StoreAdmin;
use App\Models\User;
use Illuminate\Http\Request;
use stdClass;
use Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;

class AccountController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
        $this->middleware(
            'auth'
        );
    }

    public function profile()
    {
        $auth = Auth::user();
        $response = new StdClass();
        $response->code = 200;
        $response->message = 'success';
        $response->results = $auth;

        return response()->json($response, 200);
    }

    public function update(Request $request)
    {
        $results = new StdClass();
        $customer = Auth::user();
        if ($request->userType == 'store_admin') {
            $customer = StoreAdmin::find($customer->id);
        }

        if ($request->current_password || $request->new_password) {
            $validator = Validator::make($request->all(), [
                'current_password' => 'required|min:8',
                'new_password' => 'required|min:8',
                'new_password_confirm' => 'required|same:new_password',
            ]);

            if ($validator->fails()) {
                $results->code = 203;
                $results->error = $validator->errors();
                return response()->json(['error' => $results], 203);
            }

            if (!Hash::check($request->current_password, $customer->password)) {
                $results->code = 203;
                $results->error = [
                    'current_password' => 'current password not match'
                ];
                return response()->json(['error' => $results], 203);
            }

            $customer->password = Hash::make($request->new_password);
        }

        $customer->name = $request->name ? $request->name : $customer->name;
        $customer->phone_number = $request->phone_number ? $request->phone_number : $customer->phone_number;
        $customer->save();
        if ($request->userType == 'guest') {
            $customer->fill([
                'address' => !is_null($request->address) ?
                    $this->IsEmptyString($request->address) : $customer->address,
                'longitude' => !is_null($request->longitude) ?
                    $this->IsEmptyString($request->longitude) : $customer->longitude,
                'latitude' => !is_null($request->latitude) ?
                    $this->IsEmptyString($request->latitude) : $customer->latitude,
                'province_id' => !is_null($request->province_id) ?
                    $this->IsEmptyString($request->province_id) :
                    $customer->province_id,
                'city_id' => !is_null($request->city_id) ?
                    $this->IsEmptyString($request->city_id) : $customer->city_id,
                'district_id' => !is_null($request->district_id) ?
                    $this->IsEmptyString($request->district_id) : $customer->district_id,
                'village_id' => !is_null($request->village_id) ?
                    $this->IsEmptyString($request->village_id) : $customer->village_id,
                'zip_code' => !is_null($request->zip_code) ?
                    $this->IsEmptyString($request->zip_code) : $request->zip_code,
            ]);

            $customer->save();
        }

        $results->code = 200;
        $results->message = 'success';
        $results->results = $customer;
        return response()->json($results, 200);
    }

    public function activate(Request $request)
    {
        $customer = Auth::user();
        $response = new StdClass();
        $response->code = 200;
        $response->message = 'success';
        if (
            !$customer ||
            $customer->is_active ||
            !Hash::check($request->verification_code, $customer->verification_code)
        ) {
            $response->code = 406;
            $response->message = 'Invalid code';
            return response()->json(['error' => $response], 406);
        }

        $customer->is_active = 1;
        $customer->save();

        Mail::send('account_welcome', ['customer' => $customer], function ($message) use ($customer) {
            $message->to($customer->email, $customer->name)->subject("Welcome!");
        });

        return response()->json($response);
    }

    public function updateAvatar(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'avatar' => 'required',
        ]);

        if ($validator->fails()) {
            $results = new StdClass();
            $results->code = 203;
            $results->error = $validator->errors();
            return response()->json(['error' => $results], 203);
        }

        $avatar_path = config('storage.user_avatar');
        if (!File::exists($avatar_path))
            File::makeDirectory($avatar_path, 0777, true);

        $response = new stdClass();

        if (isGuest($request)) {
            $auth = Auth::user();

            $avatar = $request->avatar;  // your base64 encoded
            $ext = explode(';base64', $avatar);
            $ext = explode('/', $ext[0]);
            $ext = $ext[1];

            $file_name = generateRandomString(70) . '.' . $ext;
            File::put($avatar_path . '/' . $file_name, file_get_contents($avatar));
            $auth->avatar = $file_name;
            $auth->save();

            $response->code = 200;
            $response->results = $auth;

            return response()->json($response, 200);
        }

        $response->code = 401;
        $response->message = 'Unauthorized';

        return response()->json(['error' => $response], 401);
    }

    function IsEmptyString($val)
    {
        if (trim($val) === '') {
            $val = NULL;
        }
        return $val;
    }
}
