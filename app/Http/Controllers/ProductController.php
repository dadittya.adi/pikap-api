<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\ProductImage;
use App\Models\Store;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use stdClass;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
        $this->middleware('auth', ['except' => [
            'lists', 'detail', 'recommended'
        ]]);
    }

    public function lists(Request $request)
    {
        $count = $request->count ? $request->count : 10;
        $page = $request->page ? $request->page : 1;
        $skip = (intval($page) - 1) * $count;

        $products = new Product();
        $products = $products->orderBy('created_at', 'DESC')->offset($skip)->limit($count)->get();

        $results = new StdClass();

        if (!count($products)) {
            $results->code = 204;
            $results->message = 'No data found';
            return response()->json($results, 200);
        }

        $results->code = 200;
        $results->results = $products;
        return response()->json($results, 200);
    }

    public function detail($id)
    {
        $product = Product::find($id);
        $results = new StdClass();

        if (!$product) {
            $results->code = 204;
            $results->message = 'No data found';
            return response()->json($results, 200);
        }

        foreach ($product->productImages as $productImage) {
            # code...
            $productImage->image = route('media.product', ['filename' => $productImage->image]);
        }
        $product->images = $product->productImages;
        unset($product->productImages);

        $results->code = 204;
        $results->results = $product;
        return response()->json($results, 200);
    }

    public function addProduct(Request $request, $store_id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:191',
            'wight' => 'numeric',
            'sku' => 'required',
            'brand' => 'required',
            'type' => 'required',
            'size' => 'required',
            'description' => 'required',
            'price' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            $results = new StdClass();
            $results->code = 203;
            $results->error = $validator->errors();
            return response()->json($results, 203);
        }

        $checkSkuProduct = Product::where('sku', $request->sku)->first();
        if ($checkSkuProduct) {
            $results = new stdClass();
            $results->code = 203;
            $results->error = [
                'sku' => ['Sku already used']
            ];
            return response()->json($results, 203);
        }

        $upload_path = config('storage.product_image');
        if (!File::exists($upload_path)) File::makeDirectory($upload_path, 0777, true);

        DB::beginTransaction();
        $store = Store::find($store_id);
        $auth = Auth::user();
        if ($store && $auth->store_id == $store->id) {
            $product = new Product();
            $product->fill([
                'store_id' => $store->id,
                'name' => $request->name,
                'weight' => $request->weight,
                'sku' => $request->sku,
                'brand' => $request->brand,
                'type' => $request->type,
                'size' => $request->size,
                'description' => $request->description,
                'price' => $request->price,
                'stock' => $request->stock || 0,
            ]);

            $product->save();
            // $images = [];
            // if ($files = $request->file('images')) {
            //   foreach ($files as $file) {
            //     do {
            //       $image = generateRandomString(70) . '.' . $file->getClientOriginalExtension();
            //     } while (ProductImage::where('image', '=', $image)->first() != null);

            //     $file->move($upload_path, $image);
            //     $productImage = new ProductImage();
            //     $productImage->fill([
            //       'product_id' => $product->id,
            //       'image' => $image
            //     ]);
            //     $productImage->save();
            //     $images[] = route('media.product', ['filename' => $productImage->image]);
            //   }
            // }
            $images = [];
            foreach ($request->images as $image) {
                # code...
                $ext = explode(';base64', $image);
                $ext = explode('/', $ext[0]);
                $ext = $ext[1];

                if ($ext != 'private') {
                    do {
                        $image_name = generateRandomString(70) . '.' . $ext;
                    } while (ProductImage::where('image', $image_name)->first());

                    File::put($upload_path . '/' . $image_name, file_get_contents($image));

                    $productImage = new ProductImage();
                    $productImage->fill([
                        'product_id' => $product->id,
                        'image' => $image_name
                    ]);
                    $productImage->save();
                    $images[] = route('media.product', ['filename' => $productImage->image]);
                }
            }
            DB::commit();
            $product->images = $images;
            $response = new StdClass();
            $response->code = 200;
            $response->result = $product;

            return response()->json($response, 200);
        }

        $response = new StdClass();
        $response->code = 401;
        $response->message = 'Unauthorized';

        return response()->json($response, 401);
    }

    public function editProduct(Request $request, $id)
    {
        $auth = Auth::user();
        $product = Product::find($id);
        $response = new StdClass();

        if (!$product) {
            $response->code = 203;
            $response->message = 'No data found';

            return response()->json($response, 200);
        }

        $validator = Validator::make($request->all(), [
            'name' => 'required|max:191',
            'description' => 'required',
            'price' => 'required|numeric',
            'stock' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            $results = new StdClass();
            $results->code = 203;
            $results->error = $validator->errors();
            return response()->json(['error' => $results], 203);
        }

        $checkSkuProduct = Product::where('sku', $request->sku)->where('id', '!=', $id)->first();
        if ($checkSkuProduct) {
            $results = new stdClass();
            $results->code = 203;
            $results->error = [
                'sku' => ['Sku already used']
            ];
            return response()->json($results, 203);
        }

        if ($product->store_id == $auth->store_id) {
            DB::beginTransaction();
            $removeImages = explode(',', $request->remove_images);
            $product->fill([
                'name' => $request->name,
                'weight' => !is_null($request->weight) ?
                    $this->IsEmptyString($request->weight) : $product->weight,
                'sku' => !is_null($request->sku) ?
                    $this->IsEmptyString($request->sku) : $product->sku,
                'brand' => !is_null($request->brand) ?
                    $this->IsEmptyString($request->brand) : $product->brand,
                'type' => !is_null($request->type) ?
                    $this->IsEmptyString($request->type) : $product->type,
                'size' => !is_null($request->size) ?
                    $this->IsEmptyString($request->size) : $product->size,
                'description' => !is_null($request->description) ?
                    $this->IsEmptyString($request->description) : $product->description,
                'price' => $request->price,
                'stock' => $request->stock,
            ]);
            $product->save();

            $product_image_path = config('storage.product_image');
            foreach ($removeImages as $value) {
                # code...
                $productImage = $product->productImages->find(trim($value));
                if ($productImage) {
                    File::delete($product_image_path . '/' . $productImage->image);
                    $productImage->delete();
                }
            }

            foreach ($request->images as $image) {
                # code...
                $ext = explode(';base64', $image);
                $ext = explode('/', $ext[0]);
                $ext = $ext[1];

                do {
                    $image_name = generateRandomString(70) . '.' . $ext;
                } while (ProductImage::where('image', $image_name)->first());

                File::put($product_image_path . '/' . $image_name, file_get_contents($image));

                $productImage = new ProductImage();
                $productImage->fill([
                    'product_id' => $product->id,
                    'image' => $image_name
                ]);
                $productImage->save();
            }

            $images = [];
            foreach ($product->productImages()->get() as $value) {
                # code...
                $value->image = route('media.product', ['filename' => $value->image]);
                $images[] = $value;
            }
            DB::commit();
            unset($product->productImages);
            $product->images = $images;
            $response->code = 200;
            $response->result = $product;

            return response()->json($response, 200);
        }

        $response->code = 401;
        $response->message = 'Unauthorized';

        return response()->json($response, 401);
    }

    public function recommended(Request $request)
    {
        $count = $request->count ? $request->count : 5;

        $products = new Product();
        if ($request->product_id) {
            $products = Product::where('id', '!=', $request->product_id);
        }

        $products = $products->inRandomOrder()->limit($count)->get();

        $results = new StdClass();

        if (!count($products)) {
            $results->code = 204;
            $results->message = 'No data found';
            return response()->json($results, 200);
        }

        $results->code = 200;
        $results->results = $products;
        return response()->json($results, 200);
    }

    function IsEmptyString($val)
    {
        if (trim($val) === '') {
            $val = NULL;
        }
        return $val;
    }
}
