<?php

use Illuminate\Database\Seeder;

use Carbon\Carbon;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $user = new User();
    $user->name = 'user';
    $user->email = 'user@example.com';
    $user->password = Hash::make('password');
    $user->address = 'address';
    $user->phone_number = 91992;
    $user->save();
  }
}
