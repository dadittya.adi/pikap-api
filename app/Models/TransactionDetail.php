<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TransactionDetail extends Model
{
  protected $table = 'transaction_details';
  protected $fillable = [
    'transaction_id',
    'store_id',
    'transaction_number',
    'pickup_code',
    'is_paid',
    'rating',
    'status',
    'subtotal',
    'shipping_type',
    'shipping_price',
    'total_amount',
  ];
  protected $appends = ['store_name', 'customer'];

  public function transaction()
  {
    return $this->belongsTo('App\Models\Transaction', 'transaction_id');
  }

  public function items()
  {
    return $this
      ->belongsToMany('App\Models\Product', 'transaction_items', 'transaction_detail_id', 'product_id')
      ->withPivot(['quantity', 'price', 'total_amount', 'notes', 'transaction_id']);
  }

  public function store()
  {
    return $this->belongsTo('App\Models\Store', 'store_id');
  }

  public function getStoreNameAttribute()
  {
    return $this->store()->first() ? $this->store()->first()->name : null;
  }

  public function getCustomerAttribute()
  {
    return $this->transaction()->first() ? $this->transaction()->first()->customer_name : null;
  }
}
