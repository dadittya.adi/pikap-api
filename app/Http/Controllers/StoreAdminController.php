<?php

namespace App\Http\Controllers;

use App\Models\Store;
use App\Models\StoreAdmin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Models\UserLevel;
use Illuminate\Support\Facades\Hash;
use stdClass;
use Validator;

class StoreAdminController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    //
    $this->middleware('auth');
  }

  public function lists(Request $request)
  {
    $isSuperAdmin = isStoreSuperAdmin();
    if ($isSuperAdmin) {
      $auth = Auth::user();
      $admin = StoreAdmin::find($auth->id);
      $store = $admin->store;
      if ($store) {
        $storeAdmins = $store->storeAdmins();
        if ($request->keyword && $request->level_id) {
          $storeAdmins = $storeAdmins
            ->where('user_level_id', $request->level_id)
            ->where('name', 'like', "%{$request->keyword}%");
        } else if ($request->keyword) {
          $storeAdmins = $storeAdmins
            ->where('name', 'like', "%{$request->keyword}%");
        } else if ($request->level_id) {
          $storeAdmins = $storeAdmins
            ->where('user_level_id', $request->level_id);
        }
        $storeAdmins = $storeAdmins->get();

        if (!count($storeAdmins)) {
          $response = new StdClass();
          $response->code = 204;
          $response->message = 'No data found';

          return response()->json($response, 200);
        }

        foreach ($storeAdmins as $storeAdmin) {
          # code...
          $storeAdmin->user_level = $storeAdmin->userLevel;
        }
        $response = new StdClass();
        $response->code = 200;
        $response->meta = [
          'store_id' => $store->id,
        ];
        $response->results = $storeAdmins;

        return response()->json($response, 200);
      }
    }

    $response = new StdClass();
    $response->code = 401;
    $response->message = 'Unauthorized';

    return response()->json($response, 401);
  }

  public function updateStoreAdmins(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'name' => 'required|max:255',
      'email' => 'required|email|unique:users,email|unique:store_admins,email,' . $request->user_id,
      'phone_number' => 'required|max:20',
      'user_level_id' => 'numeric',
      'store_id' => 'numeric',
      'password' => $request->user_id ? '' : 'required',
    ]);

    if ($validator->fails()) {
      $results = new StdClass();
      $results->code = 203;
      $results->error = $validator->errors();
      return response()->json($results, 203);
    }

    $isSuperAdmin = isStoreSuperAdmin();
    if ($isSuperAdmin) {
      $auth = Auth::user();
      $store = Store::find($auth->store_id);
      $storeAdmin = new StoreAdmin();
      if ($request->user_id) {
        $storeAdmin = $store->storeAdmins->find($request->user_id);
        if (!$storeAdmin) {
          $response = new StdClass();
          $response->code = 401;
          $response->message = 'Unauthorized';

          return response()->json($response, 401);
        }
      } else {
        $storeAdmin->password = Hash::make($request->password);
      }

      $storeAdmin->name = $request->name;
      $storeAdmin->email = $request->email;
      $storeAdmin->phone_number = $request->phone_number;
      $storeAdmin->user_level_id = $request->user_level_id;
      $storeAdmin->store_id = $store->id;
      $storeAdmin->save();

      $response = new StdClass();
      $response->code = 200;
      $response->results = $storeAdmin;

      return response()->json($response, 401);
    }

    $response = new StdClass();
    $response->code = 401;
    $response->message = 'Unauthorized';

    return response()->json($response, 401);
  }
}
