<?php

use Illuminate\Database\Seeder;
use App\Models\UserLevel;

class UserLevelSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    //
    $types = ['SUPERADMIN', 'ADMIN', 'CASHIER'];
    foreach ($types as $type) {
      $userLevel = new UserLevel();
      $userLevel->name = $type;
      $userLevel->save();
    }
  }
}
