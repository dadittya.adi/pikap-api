<!DOCTYPE html>
<html lang="en-US">

<head>
  <meta charset="utf-8">
</head>

<body>
  <h3>Your account in Max Virtual Race has been actived !</h3>

  <div>
    <p style="margin: 1em 0;">Welcome to Max Virtual Race!</p>
    <p style="margin: 1em 0;">
      Thank you for confirming your account. <br>
      Now that it has been activated, you have opened the doors to many of our products to suit your needs.
    </p>

    <p style="margin: 1em 0;">
      Thank you for your purchases in our store. <br>
      Happy shopping!
    </p>

    <p style="margin: 1em 0;">
      Cheers and Love<br>
      Max Virtual Race
    </p>

    <p style="margin: 1em 0;">If you have any inquiries, please kindly email us at info@maxvirtualrace.com.</p>
    <p style="margin: 1em 0;">We will reply you as soon as we could and please do not reply to this email.</p>

  </div>

  <div style="font-size: 11px; margin-top: 10px">
    This email was sent automatically by Max Virtual Race.
  </div>
  <br>
  <hr>

  <div>
    <p style="margin: 1em 0;">Selamat datang di Max Virtual Race!</p>
    <p style="margin: 1em 0;">
      Terima kasih telah mengkonfirmasi akun anda <br>
      Akun anda sekarang telah aktif, anda telah membuka pintu untuk semua kebutuhan anda.
    </p>

    <p style="margin: 1em 0;">
      Terima kasih untuk pembelian anda di Max Virtual Race<br>
      Selamat berolahraga!
    </p>

    <p style="margin: 1em 0;">
      Salam Hangat<br>
      Max Virtual Race
    </p>

    <p style="margin: 1em 0;">Jika anda mempunyai pertanyaan atau keluhan, silahkan email kami di info@maxvirtualrace.com atau telepon ke (024) 692 5460.</p>
    <!--
			<p style="margin: 1em 0;">Our working hours is :<br>
				Monday – Friday	: 08:00 – 16:00 WIB<br>
				Saturday		: 08:00 – 13:00 WIB
			</p>
			!-->
    <p style="margin: 1em 0;">Kami akan membalas segera dan harap jangan membalas email ini.</p>

  </div>

  <div style="font-size: 11px; margin-top: 10px">
    Email ini dikirim secara otomatis oleh Max Virtual Race.
  </div>

</body>

</html>