<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api/'], function () use ($router) {
    $router->post('auth/login[/{type}]', 'AuthController@login');
    $router->post('auth/register[/{type}]', 'AuthController@register');

    $router->get('province', 'RegionController@getProvince');
    $router->get('city/{province_id}', 'RegionController@getCity');
    $router->get('city_status/{status}', 'RegionController@getCityByStatus');
    $router->get('district/{city_id}', 'RegionController@getDistrict');
    $router->get('village/{district_id}', 'RegionController@getVillage');

    $router->get('user_level', 'UserLevelController@lists');

    $router->post('check_email[/{type}]', 'EtcController@checkEmail');
    $router->post('check_store_name', 'EtcController@checkStoreName');
});

$router->group(['prefix' => 'api/store/'], function () use ($router) {
    $router->get('store_admin', 'StoreAdminController@lists');
    $router->post('store_admin/update', 'StoreAdminController@updateStoreAdmins');

    $router->post('edit', 'StoreController@update');
    $router->get('detail/{id}', 'StoreController@detail');
    $router->get('lists', 'StoreController@lists');
    $router->get('{id}/product', 'StoreController@productList');
    $router->get('recommended', 'StoreController@recommended');
});

$router->group(['prefix' => 'api/product/'], function () use ($router) {
    $router->post('{store_id}/add', 'ProductController@addProduct');
    $router->post('{id}/edit', 'ProductController@editProduct');
    $router->get('lists', 'ProductController@lists');
    $router->get('detail/{id}', 'ProductController@detail');
    $router->get('recommended', 'ProductController@recommended');
});


$router->group(['prefix' => 'api/account/'], function () use ($router) {
    $router->post('activate', 'AccountController@activate');
    $router->get('profile', 'AccountController@profile');

    $router->post('profile/update', 'AccountController@update');
    $router->post('profile/avatar', 'AccountController@updateAvatar');
});

$router->group(['prefix' => 'api/user/'], function () use ($router) {
    $router->post('follow', 'UserController@followStore');
    $router->post('unfollow', 'UserController@unfollowStore');

    $router->post('like', 'UserController@likeProduct');
    $router->post('unlike', 'UserController@unlikeProduct');

    $router->get('followed_stores', 'UserController@followedStoreLists');
    $router->get('liked_product', 'UserController@likedProductLists');
    $router->get('followed_store/product_lists', 'UserController@productLists');
});

$router->group(['prefix' => 'api/user/cart'], function () use ($router) {
    $router->get('lists', 'CartController@lists');
    $router->get('count', 'CartController@count');
    $router->post('add/{product_id}', 'CartController@add');
    $router->post('edit/{product_id}', 'CartController@edit');
    $router->delete('delete/{product_id}', 'CartController@delete');
    $router->delete('delete_all', 'CartController@deleteAll');
});

$router->group(['prefix' => 'api/user/transaction'], function () use ($router) {
    $router->get('lists', 'UserTransactionController@lists');
    $router->get('detail/{id}', 'UserTransactionController@details');

    $router->post('checkout', 'UserTransactionController@checkout');
    $router->post('payment/{id}', 'UserTransactionController@payment');
});

$router->group(['prefix' => 'api/merchant/transaction'], function () use ($router) {
    $router->get('lists', 'MerchantTransactionController@lists');
    $router->get('detail/{id}', 'MerchantTransactionController@details');
    $router->get('statistic', 'MerchantTransactionController@statistics');
    $router->get('detail/code/{code}', 'MerchantTransactionController@detailUseCode');
    $router->post('edit/{id}', 'MerchantTransactionController@update');
});

$router->group(['prefix' => 'media/'], function () use ($router) {
    $router->get('product/{filename}', ['as' => 'media.product', 'uses' => 'MediaController@product']);
    $router->get('banner/{filename}', ['as' => 'media.store_banner', 'uses' => 'MediaController@storeBanner']);
    $router->get('avatar/{filename}', ['as' => 'media.store_avatar', 'uses' => 'MediaController@storeAvatar']);
    $router->get('proof_of_payment/{filename}', ['as' => 'media.proof_of_payment', 'uses' => 'MediaController@proofOfPayment']);
    $router->get('user_avatar/{filename}', ['as' => 'media.user_avatar', 'uses' => 'MediaController@userAvatar']);
});
