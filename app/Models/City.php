<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
  //
  protected $table = 'cities';
  protected $fillable = ['province_id', 'name'];

  public function province()
  {
    return $this->belongsTo('App\Models\Province', 'province_id');
  }

  public function districts()
  {
    return $this->hasMany('App\Models\District', 'city_id');
  }
}
