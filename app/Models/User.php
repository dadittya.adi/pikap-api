<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'phone_number',
        'address',
        'longitude',
        'latitude',
        'province_id',
        'city_id',
        'district_id',
        'village_id',
        'zip_code',
        'verification_code',
        'email_verified_at',
        'token',
        'last_active',
        'is_active',
        'fcm_token',
        'avatar'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'token',
        'verification_code',
        'email_verified_at',
        'avatar'
    ];

    protected $appends = ['avatar_url'];

    public function likedProducts()
    {
        return $this->belongsToMany('App\Models\Product', 'likes', 'user_id', 'product_id');
    }

    public function followedStores()
    {
        return $this->belongsToMany('App\Models\Store', 'follows', 'user_id', 'store_id');
    }

    public function transactions()
    {
        return $this->hasMany('App\Models\Transaction', 'user_id');
    }

    public function carts()
    {
        return $this
            ->belongsToMany('App\Models\Product', 'carts', 'user_id', 'product_id')
            ->withPivot(['quantity', 'notes']);
    }

    public function generateConfirmHash()
    {
        $s = hash_hmac('sha1', $this->email, $this->created_at, true);
        $s = base64_encode($s);
        $s = strtr($s, array('+' => '-', '/' => '_', '=' => ''));
        return $s;
    }

    public function checkConfirmHash($hash)
    {
        $s = hash_hmac('sha1', $this->email, $this->created_at, true);
        $s = base64_encode($s);
        $s = strtr($s, array('+' => '-', '/' => '_', '=' => ''));
        return $s == $hash;
    }

    public function getAvatarUrlAttribute()
    {
        $avatar = $this->avatar;
        $image = url('/') . '/images/avatar_default.png';
        if ($avatar)
            $image = route('media.user_avatar', ['filename' => $avatar]);

        return $image;
    }
}
