<?php

namespace App\Http\Controllers;

use App\Models\Merchant;
use App\Models\Product;
use App\Models\Store;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use stdClass;
use Validator;

class StoreController extends Controller
{

    public function lists(Request $request)
    {
        $count = $request->count ? $request->count : 10;
        $page = $request->page ? $request->page : 1;
        $skip = (intval($page) - 1) * $count;
        $response = new StdClass();

        $stores = Store::Where('name', 'like', '%' . $request->keyword . '%')
            ->offset($skip)->limit($count)->get();

        if (!count($stores)) {
            $response->code = 204;
            $response->message = 'No data found';

            return response()->json(['error' => $response], 200);
        }

        foreach ($stores as $key => $value) {
            # code...
            if ($value->avatar)
                $value->avatar = route('media.store_avatar', ['filename' => $value->avatar]);
            else
                $value->avatar = url('/') . '/images/default.jpg';

            if ($value->banner)
                $value->banner = route('media.store_banner', ['filename' => $value->banner]);
            else
                $value->banner = url('/') . '/images/default.jpg';
        }

        $response->code = 200;
        $response->results = $stores;

        return response()->json($response, 200);
    }

    public function detail(Request $request, $id)
    {
        $store = Store::find($id);
        $response = new StdClass();

        if (!$store) {
            $response->code = 204;
            $response->message = 'No data found';

            return response()->json(['error' => $response], 200);
        }

        if ($store->avatar)
            $store->avatar = route('media.store_avatar', ['filename' => $store->avatar]);
        else
            $store->avatar = url('/') . '/images/default.jpg';

        if ($store->banner)
            $store->banner = route('media.store_banner', ['filename' => $store->banner]);
        else
            $store->banner = url('/') . '/images/default.jpg';

        $response->code = 200;
        $response->results = $store;

        return response()->json($response, 200);
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'phone_number' => 'required|max:20',
            'province_id' => 'sometimes|numeric',
            'city_id' => 'sometimes|numeric',
            'district_id' => 'sometimes|numeric',
            'village_id' => 'sometimes|numeric',
            'zip_code' => 'sometimes|numeric',
            'facebook_url' => 'url',
            'twitter_url' => 'url',
            'instagram_url' => 'url',
            'address' => 'required',
            'order_processing_time' => 'sometimes|numeric',
        ]);

        if ($validator->fails()) {
            $results = new StdClass();
            $results->code = 203;
            $results->error = $validator->errors();
            return response()->json(['error' => $results], 203);
        }
        $banner_path = config('storage.banner');
        if (!File::exists($banner_path))
            File::makeDirectory($banner_path, 0777, true);

        $avatar_path = config('storage.store_avatar');
        if (!File::exists($avatar_path))
            File::makeDirectory($avatar_path, 0777, true);

        $auth = Auth::user();
        $merchant = Merchant::where('email', $auth->email)->first();
        if ($merchant) {
            $store = $merchant->stores->first();
            $banner_name = $store->banner;
            $avatar_name = $store->avatar;

            if ($request->banner) {
                $banner = $request->banner;  // your base64 encoded
                $ext = explode(';base64', $banner);
                $ext = explode('/', $ext[0]);
                $ext = $ext[1];

                $banner_name = generateRandomString(70) . '.' . $ext;
                File::put($banner_path . '/' . $banner_name, file_get_contents($banner));
            }

            if ($request->avatar) {
                $avatar = $request->avatar;  // your base64 encoded
                $ext = explode(';base64', $avatar);
                $ext = explode('/', $ext[0]);
                $ext = $ext[1];

                $avatar_name = generateRandomString(70) . '.' . $ext;
                File::put($avatar_path . '/' . $avatar_name, file_get_contents($avatar));
            }

            $store->name = $request->name;
            $store->avatar = $avatar_name;
            $store->banner = $banner_name;
            $store->facebook_url = $request->facebook_url;
            $store->twitter_url = $request->twitter_url;
            $store->instagram_url = $request->instagram_url;
            $store->address = $request->address;
            $store->province_id = $request->province_id;
            $store->city_id = $request->city_id;
            $store->district_id = $request->district_id;
            $store->village_id = $request->village_id;
            $store->longitude = $request->longitude;
            $store->latitude = $request->latitude;
            $store->zip_code = $request->zip_code;
            $store->phone_number = $request->phone_number;
            $store->order_processing_time = $request->order_processing_time;
            $store->description = $request->description;
            $store->save();

            if ($store->banner) {
                $store->banner = route('media.store_banner', ['filename' => $store->banner]);
            }

            if ($store->avatar) {
                $store->avatar = route('media.store_avatar', ['filename' => $store->avatar]);
            }

            $response = new StdClass();
            $response->code = 200;
            $response->results = $store;

            return response()->json($response, 200);
        }

        $response = new StdClass();
        $response->code = 401;
        $response->message = 'Unauthorized';

        return response()->json(['error' => $response], 401);
    }

    public function productList(Request $request, $id)
    {
        $store = Store::find($id);
        $response = new StdClass();

        if (!$store) {
            $response->code = 204;
            $response->message = 'No data found';

            return response()->json(['error' => $response], 200);
        }

        $count = $request->count ? $request->count : 10;
        $page = $request->page ? $request->page : 1;
        $skip = (intval($page) - 1) * $count;

        $products = $store->products()->offset($skip)->limit($count)->get();

        if (!count($products)) {
            $response->code = 204;
            $response->message = 'No data found';

            return response()->json(['error' => $response], 200);
        }

        $response->code = 200;
        $response->results = $products;

        return response()->json($response, 200);
    }

    public function recommended(Request $request)
    {
        $count = $request->count ? $request->count : 5;

        $stores = new Store();
        if ($request->store_id) {
            $stores = Store::where('id', '!=', $request->store_id);
        } else if ($request->product_id) {
            $product = Product::find($request->product_id);
            if ($product)
                $stores = Store::where('id', '!=', $product->store_id);
        }

        $stores = $stores->inRandomOrder()->limit($count)->get();

        foreach ($stores as $key => $value) {
            # code...
            if ($value->avatar)
                $value->avatar = route('media.store_avatar', ['filename' => $value->avatar]);
            else
                $value->avatar = url('/') . '/images/default.jpg';

            if ($value->banner)
                $value->banner = route('media.store_banner', ['filename' => $value->banner]);
            else
                $value->banner = url('/') . '/images/default.jpg';
        }

        $results = new StdClass();

        if (!count($stores)) {
            $results->code = 204;
            $results->message = 'No data found';
            return response()->json($results, 200);
        }

        $results->code = 200;
        $results->results = $stores;
        return response()->json($results, 200);
    }
}
