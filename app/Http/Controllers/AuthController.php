<?php

namespace App\Http\Controllers;

use App\Jobs\SendConfirmationEmailJob;
use App\Jobs\StoreAdminConfirmationEmailJob;
use App\Models\Merchant;
use App\Models\Store;
use App\Models\StoreAdmin;
use App\Models\User;
use App\Models\UserLevel;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use stdClass;
use Validator;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Queue;

class AuthController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function login(Request $request, $type = null)
    {
        $validator = Validator::make($request->all(), [
            'identity' => 'required',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            $results = new StdClass();
            $results->code = 203;
            $results->error = $validator->errors();
            return response()->json(['error' => $results], 203);
        }

        $field = $this->checkField();

        if ($type === 'store') {
            $user = StoreAdmin::where($field, $request->identity)->first();
        } else {
            $user = User::where($field, $request->identity)->first();
        }
        if ($user && Hash::check($request->password, $user->password)) {
            $genToken = $this->generateRandomString();
            do {
                $genToken = $this->generateRandomString();
            } while (User::where('token', $genToken)->first());
            $user->token = $genToken;
            $user->fcm_token = $request->tokenFcm;
            $user->save();

            if ($type == 'store') {
                $user->store;
            }

            $response = [
                "message" => "success",
                "code"    => 200,
                'meta' => [
                    'token' => $genToken
                ],
                'results' => $user
            ];

            return response()->json($response, 200);
        } else {
            $response = [
                "message" => "The credentials do not match our records",
                "code"    => 401,
            ];
            return response()->json(['error' => $response], 401);
        }
    }

    public function checkField()
    {
        $login = request()->input('identity');

        $field = filter_var($login, FILTER_VALIDATE_EMAIL) ? 'email' : 'phone_number';
        request()->merge([$field => $login]);

        return $field;
    }

    public function register(Request $request, $type = null)
    {
        DB::beginTransaction();
        $name_validator = 'max:255';
        if ($type == 'store')
            $name_validator .= 'required|unique:merchants,name';

        $validation = [];
        $validation['password'] = 'required|min:8';
        $validation['password_confirm'] = 'required|same:password';
        $validation['name'] = $name_validator;
        $email = '';
        $phone_number = '';
        $field = $this->checkField();
        if ($field == 'email') {
            $validation['identity'] = 'required|email|unique:users,email';
            if ($type == 'store') {
                $validation['identity'] = 'required|email|unique:store_admins,email';
            }
            $email = $request->identity;
        } else {
            $validation['identity'] = 'required|unique:users,phone_number';
            if ($type == 'store') {
                $validation['identity'] = 'required|unique:store_admins,phone_number';
            }
            $phone_number = $request->identity;
        }

        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails()) {
            $results = new StdClass();
            $results->code = 203;
            $results->error = $validator->errors();
            return response()->json(['results' => $results], 203);
        }

        $user = new stdClass();
        $verification_code = rand(1000, 9999);
        $genToken = $this->generateRandomString();
        do {
            $genToken = $this->generateRandomString();
        } while (
            User::where('token', $genToken)->first() || StoreAdmin::where('token', $genToken)->first()
        );
        if (!$type) {
            $user = new User();
            $user->fill([
                'name' => $request->name,
                'email' => $email,
                'password' => Hash::make($request->password),
                'phone_number' => $phone_number,
                'address' => $request->address,
                'longitude' => $request->longitude,
                'latitude' => $request->latitude,
                'province_id' => $request->province_id,
                'city_id' => $request->city_id,
                'district_id' => $request->district_id,
                'village_id' => $request->village_id,
                'zip_code' => $request->zip_code,
                'verification_code' => Hash::make($verification_code),
                'token' => $genToken,
            ]);
            if ($user->save() && $field == 'email') {
                $user->verification_code_unhash = $verification_code;

                $job = (new SendConfirmationEmailJob($user, $verification_code))->onQueue('emails');
                $this->dispatch($job);
            }
        } else if ($type === 'store') {
            $merchant = new Merchant();
            $merchant->fill([
                'name' => $request->name,
                'email' => $email,
                'password' => Hash::make($request->password),
            ]);
            $merchant->save();

            $store = new Store();
            $store->fill([
                'name' => $request->name,
                'email' => $email,
                'merchant_id' => $merchant->id,
                'phone_number' => $phone_number,
                'address' => $request->address,
                'longitude' => $request->longitude,
                'latitude' => $request->latitude,
                'province_id' => $request->province_id,
                'city_id' => $request->city_id,
                'district_id' => $request->district_id,
                'village_id' => $request->village_id,
                'zip_code' => $request->zip_code,
                'area_id' => $request->area_id,
            ]);
            $store->save();

            $userLevel = UserLevel::where('name', 'SUPERADMIN')->first();
            $storeAdmin = new StoreAdmin();
            $storeAdmin->fill([
                'store_id' => $store->id,
                'user_level_id' => $userLevel->id,
                'email' => $email,
                'password' => Hash::make($request->password),
                'phone_number' => $phone_number,
                'token' => $genToken,
                'name' => $request->name,
                'verification_code' => Hash::make($verification_code),
            ]);
            if ($storeAdmin->save() && $field == 'email') {
                $job = (new StoreAdminConfirmationEmailJob($storeAdmin, $verification_code))->onQueue('emails');
                $this->dispatch($job);
            }
            $user = $store;
            $user->store_id = $store->id;
            $user->id = $merchant->id;
        }
        DB::commit();

        $result = [
            "message" => "register_success",
            "code"    => 200,
            'meta' => [
                'token' => $genToken
            ],
            'results' => $user
        ];

        return response()->json($result);
    }

    function generateRandomString($length = 128)
    {
        $char = '012345678dssd9abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charLength = strlen($char);
        $str = '';
        for ($i = 0; $i < $length; $i++) {
            $str .= $char[rand(0, $charLength - 1)];
        }
        return $str;
    }
}
