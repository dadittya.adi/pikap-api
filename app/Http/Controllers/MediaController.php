<?php

namespace App\Http\Controllers;

class MediaController extends Controller
{
    public function product($filename)
    {
        $path = config('storage.product_image');
        $resp = response()->download($path . '/' . $filename);
        $resp->headers->set('Content-Disposition', 'inline');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');

        //disable cache
        $resp->headers->set('Cache-Control', 'no-cache, no-store, must-revalidate'); // HTTP 1.1.
        $resp->headers->set('Pragma', 'no-cache'); // HTTP 1.0.
        $resp->headers->set('Expires', '0'); // Proxies.

        return $resp;
    }

    public function storeBanner($filename)
    {
        $path = config('storage.banner');
        $resp = response()->download($path . '/' . $filename);
        $resp->headers->set('Content-Disposition', 'inline');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');

        //disable cache
        $resp->headers->set('Cache-Control', 'no-cache, no-store, must-revalidate'); // HTTP 1.1.
        $resp->headers->set('Pragma', 'no-cache'); // HTTP 1.0.
        $resp->headers->set('Expires', '0'); // Proxies.

        return $resp;
    }

    public function storeAvatar($filename)
    {
        $path = config('storage.store_avatar');
        $resp = response()->download($path . '/' . $filename);
        $resp->headers->set('Content-Disposition', 'inline');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');

        //disable cache
        $resp->headers->set('Cache-Control', 'no-cache, no-store, must-revalidate'); // HTTP 1.1.
        $resp->headers->set('Pragma', 'no-cache'); // HTTP 1.0.
        $resp->headers->set('Expires', '0'); // Proxies.

        return $resp;
    }

    public function proofOfPayment($filename)
    {
        $path = config('storage.proof_of_payment');
        $resp = response()->download($path . '/' . $filename);
        $resp->headers->set('Content-Disposition', 'inline');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');

        //disable cache
        $resp->headers->set('Cache-Control', 'no-cache, no-store, must-revalidate'); // HTTP 1.1.
        $resp->headers->set('Pragma', 'no-cache'); // HTTP 1.0.
        $resp->headers->set('Expires', '0'); // Proxies.

        return $resp;
    }

    public function userAvatar($filename)
    {
        $path = config('storage.user_avatar');
        $resp = response()->download($path . '/' . $filename);
        $resp->headers->set('Content-Disposition', 'inline');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');

        //disable cache
        $resp->headers->set('Cache-Control', 'no-cache, no-store, must-revalidate'); // HTTP 1.1.
        $resp->headers->set('Pragma', 'no-cache'); // HTTP 1.0.
        $resp->headers->set('Expires', '0'); // Proxies.

        return $resp;
    }
}
