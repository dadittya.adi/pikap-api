<?php

namespace App\Http\Controllers;

use App\Models\ProductImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use stdClass;
use Validator;

class UserController extends Controller
{

  public function __construct()
  {
    $this->middleware('auth');
  }

  public function followStore(Request $request)
  {
    $response = new StdClass();

    $validator = Validator::make($request->all(), [
      'store_id' => 'numeric|required',
    ]);

    if ($validator->fails()) {
      $results = new StdClass();
      $results->code = 203;
      $results->error = $validator->errors();
      return response()->json(['error' => $results], 203);
    }
    if (isGuest($request)) {
      $auth = Auth::user();
      $auth->followedStores()->attach($request->store_id);

      $response->code = 200;
      $response->message = 'Success';

      return response()->json($response, 200);
    }

    $response->code = 401;
    $response->message = 'Unauthorized';

    return response()->json(['error' => $response], 401);
  }

  public function unfollowStore(Request $request)
  {
    $response = new StdClass();

    $validator = Validator::make($request->all(), [
      'store_id' => 'numeric|required',
    ]);

    if ($validator->fails()) {
      $results = new StdClass();
      $results->code = 203;
      $results->error = $validator->errors();
      return response()->json(['error' => $results], 203);
    }
    if (isGuest($request)) {
      $auth = Auth::user();
      $auth->followedStores()->detach($request->store_id);

      $response->code = 200;
      $response->message = 'Success';

      return response()->json($response, 200);
    }

    $response->code = 401;
    $response->message = 'Unauthorized';

    return response()->json(['error' => $response], 401);
  }

  public function likeProduct(Request $request)
  {
    $response = new StdClass();

    $validator = Validator::make($request->all(), [
      'product_id' => 'numeric|required',
    ]);

    if ($validator->fails()) {
      $results = new StdClass();
      $results->code = 203;
      $results->error = $validator->errors();
      return response()->json(['error' => $results], 203);
    }
    if (isGuest($request)) {
      $auth = Auth::user();
      $auth->likedProducts()->attach($request->product_id);

      $response->code = 200;
      $response->message = 'Success';

      return response()->json($response, 200);
    }

    $response->code = 401;
    $response->message = 'Unauthorized';

    return response()->json(['error' => $response], 401);
  }

  public function unlikeProduct(Request $request)
  {
    $response = new StdClass();

    $validator = Validator::make($request->all(), [
      'product_id' => 'numeric|required',
    ]);

    if ($validator->fails()) {
      $results = new StdClass();
      $results->code = 203;
      $results->error = $validator->errors();
      return response()->json(['error' => $results], 203);
    }
    if (isGuest($request)) {
      $auth = Auth::user();
      $auth->likedProducts()->detach($request->product_id);

      $response->code = 200;
      $response->message = 'Success';

      return response()->json($response, 200);
    }

    $response->code = 401;
    $response->message = 'Unauthorized';

    return response()->json(['error' => $response], 401);
  }

  public function followedStoreLists(Request $request)
  {
    $count = $request->count ? $request->count : 10;
    $page = $request->page ? $request->page : 1;
    $skip = (intval($page) - 1) * $count;
    $response = new StdClass();

    if (isGuest($request)) {
      $auth = Auth::user();
      $stores = $auth
        ->followedStores()
        ->select('stores.id', 'stores.name', 'stores.avatar', 'stores.banner')
        ->offset($skip)
        ->limit($count)
        ->with(['products' => function ($query) {
          $query->orderByRaw('RAND()')->limit(3);
        }])
        ->get();

      if (!count($stores)) {
        $response->code = 204;
        $response->message = 'No data found';
        return response()->json(['error' => $response], 200);
      }

      foreach ($stores as $key => $store) {
        # code...
        $store->avatar = $store->avatar_image;
        $store->banner = $store->banner_image;

        unset($store->pivot);
      }

      $response->code = 200;
      $response->results = $stores;

      return response()->json($response, 200);
    }

    $response->code = 401;
    $response->message = 'Unauthorized';

    return response()->json(['error' => $response], 401);
  }

  // function untuk get product lists on followed store
  public function productLists(Request $request)
  {
    $count = $request->count ? $request->count : 10;
    $page = $request->page ? $request->page : 1;
    $skip = (intval($page) - 1) * $count;
    $response = new stdClass();

    if (isGuest($request)) {
      $auth = Auth::user();
      $query = 'SELECT product.id, product.name, product.price, store.name as store_name FROM users user ';
      $query .= 'INNER JOIN follows follow ON user.id = follow.user_id ';
      $query .= 'INNER JOIN stores store ON follow.store_id = store.id ';
      $query .= 'INNER JOIN products product ON store.id = product.store_id ';
      $query .= 'WHERE user.id = ' . $auth->id . ' ';
      $query .= 'ORDER BY product.created_at DESC ';
      $query .= 'LIMIT ' . $skip . ', ' . $count;

      $products = DB::select($query);

      foreach ($products as $key => $value) {
        # code...
        $productImage = ProductImage::where('product_id', $value->id)->first();
        if ($productImage)
          $value->image = route('media.product', ['filename' => $productImage->image]);
        else
          $value->image = url('/') . '/images/default.jpg';
      }

      $response->code = 200;
      $response->results = $products;

      return response()->json($response, 200);
    }

    $response->code = 401;
    $response->message = 'Unauthorized';

    return response()->json(['error' => $response], 401);
  }

  public function likedProductLists(Request $request)
  {
    $count = $request->count ? $request->count : 10;
    $page = $request->page ? $request->page : 1;
    $skip = (intval($page) - 1) * $count;
    $response = new StdClass();

    if (isGuest($request)) {
      $auth = Auth::user();
      $products = $auth
        ->likedProducts()
        ->select('products.id', 'products.name')
        ->offset($skip)
        ->limit($count)
        ->get();

      if (!count($products)) {
        $response->code = 204;
        $response->message = 'No data found';
        return response()->json(['error' => $response], 200);
      }

      foreach ($products as $key => $value) {
        # code...
        $productImage = $value->productImages->first();
        if ($productImage)
          $value->image = route('media.product', ['filename' => $productImage->image]);
        else
          $value->image = url('/') . '/images/default.jpg';

        unset($value->pivot);
        unset($value->productImages);
      }

      $response->code = 200;
      $response->results = $products;

      return response()->json($response, 200);
    }

    $response->code = 401;
    $response->message = 'Unauthorized';

    return response()->json(['error' => $response], 401);
  }
}
