<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StoreAdmin extends Model
{
  //
  protected $table = 'store_admins';
  protected $fillable = [
    'store_id',
    'user_level_id',
    'email',
    'password',
    'phone_number',
    'token',
    'last_active',
    'name',
    'is_active',
    'fcm_token',
  ];
  protected $hidden = [
    'password',
    'token',
  ];

  public function store()
  {
    return $this->belongsTo('App\Models\Store', 'store_id');
  }

  public function userLevel()
  {
    return $this->belongsTo('App\Models\UserLevel', 'user_level_id');
  }
}
