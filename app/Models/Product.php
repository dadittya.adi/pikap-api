<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Product extends Model
{
  protected $table = 'products';
  protected $fillable = [
    'store_id',
    'sku',
    'name',
    'weight',
    'brand',
    'type',
    'size',
    'description',
    'price',
    'stock',
    'is_active',
    'description',
  ];

  protected $appends = ['image', 'liked', 'store_name'];

  public function getLikedAttribute()
  {
    $auth = Auth::user();
    $res = false;
    if ($auth)
      $res = $this->usersLiking()->find($auth->id);
    return $res ? true : false;
  }

  public function getImageAttribute()
  {
    $productImage = $this->productImages()->first();
    $image = url('/') . '/images/default.jpg';
    if ($productImage)
      $image = route('media.product', ['filename' => $productImage->image]);

    return $image;
  }

  public function getStoreNameAttribute()
  {
    $store = $this->productStore()->first();
    $storeName = '';
    if ($store)
      $storeName = $store->name;
    return $storeName;
  }

  public function productStore()
  {
    return $this->belongsTo('App\Models\Store', 'store_id');
  }

  public function productImages()
  {
    return $this->hasMany('App\Models\ProductImage', 'product_id');
  }

  public function usersLiking()
  {
    return $this->belongsToMany('App\Models\User', 'likes', 'product_id', 'user_id');
  }

  public function carts()
  {
    return $this
      ->belongsToMany('App\Models\User', 'carts', 'product_id', 'user_id')
      ->withPivot(['quantity', 'notes']);
  }
}
