<?php

namespace App\Http\Controllers;

use App\Models\UserLevel;
use stdClass;

class UserLevelController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    //
  }

  public function lists()
  {
    $userLevel = UserLevel::get();
    $response = new StdClass();
    $response->code = 200;
    $response->message = 'success';
    $response->results = $userLevel;

    return response()->json($response, 200);
  }
}
