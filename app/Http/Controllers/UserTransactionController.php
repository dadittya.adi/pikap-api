<?php

namespace App\Http\Controllers;

use App\Models\Transaction;
use App\Models\TransactionDetail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

use stdClass;
use Validator;

class UserTransactionController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}

	public function lists(Request $request)
	{
		if (!isGuest($request)) {
			$response = new stdClass();
			$response->code = 401;
			$response->message = 'Unauthorized';

			return response()->json(['error' => $response], 401);
		}

		$validator = Validator::make($request->all(), [
			'status' => 'sometimes|in:INV,PAID,PROCESS,READY,FINISH',
		]);

		if ($validator->fails()) {
			$results = new StdClass();
			$results->code = 203;
			$results->error = $validator->errors();
			return response()->json(['error' => $results], 203);
		}

		$count = $request->count ? $request->count : 10;
		$page = $request->page ? $request->page : 1;
		$skip = (intval($page) - 1) * $count;

		$auth = Auth::user();
		$transactions = $auth->transactions();

		if ($request->status) {
			$transactions = $transactions->where('transactions.payment_status', $request->status);
		}
		$transactions = $transactions->offset($skip)->limit($count)->get();

		$results = new StdClass();

		if (!count($transactions)) {
			$results->code = 204;
			$results->message = 'No data found';
			return response()->json(['error' => $results], 200);
		}

		$results->code = 200;
		$results->results = $transactions;
		return response()->json($results, 200);
	}

	public function details(Request $request, $id)
	{
		$transaction = Transaction::find($id);
		$auth = Auth::user();

		$response = new stdClass();

		if (!$transaction) {
			$response->code = 204;
			$response->message = 'No data found';
			return response()->json(['error' => $response], 200);
		}

		if ($auth->id == $transaction->user_id && $request->userType == 'guest') {
			// $transaction = $transaction->with(['transactionDetails' => function ($query) {
			//   $query;
			// }])
			//   ->get();
			$transactionDetail = $transaction->transactionDetails()->with(['items'])->get();
			$transaction->transaction_details = $transactionDetail;

			$response->code = 200;
			$response->results = $transaction;

			return response()->json($response, 200);
		}

		$response->code = 401;
		$response->message = 'Unauthorized';

		return response()->json(['error' => $response], 401);
	}

	public function checkout(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'payment_type' => 'required',
			'source' => 'required|in:IOS,ANDROID',
			'shipping_type' => 'required|in:COD,COP,GOSEND',
			'customer_name' => 'required',
			'customer_email' => 'required',
			'customer_phone_number' => 'required',
			'customer_address' => 'required',
			'customer_province_id' => 'required',
			'customer_city_id' => 'required',
			'customer_district_id' => 'required',
			'customer_village_id' => 'required',
			'customer_zip_code' => 'required',
		]);

		$response = new stdClass();

		if ($validator->fails()) {
			$response->code = 203;
			$response->error = $validator->errors();
			return response()->json(['error' => $response], 203);
		}

		if (isGuest($request)) {
			DB::beginTransaction();
			$auth = Auth::user();
			$carts = $auth->carts()->get()->groupBy('store_id');
			$invoice_number = str_replace('-', '/', Carbon::now()->toDateString());
			$invoice_number .= '/' . strtoupper(generateRandomString(7));
			$transaction = new Transaction();
			$payment_status = 'INV';
			if ($request->shipping_type == 'COP' || $request->shipping_type == 'COD') {
				$payment_status = 'PAID';
			}
			$transaction->fill([
				'user_id' => $auth->id,
				'invoice_number' => $invoice_number,
				'date' => Carbon::now(),
				'payment_type' => $request->payment_type,
				'payment_status' => $payment_status,
				'source' => $request->source,
				'shipping_type' => $request->shipping_type,
				'customer_name' => $request->customer_name,
				'customer_email' => $request->customer_email,
				'customer_phone_number' => $request->customer_phone_number,
				'customer_address' => $request->customer_address,
				'customer_province_id' => $request->customer_province_id,
				'customer_city_id' => $request->customer_city_id,
				'customer_district_id' => $request->customer_district_id,
				'customer_village_id' => $request->customer_village_id,
				'customer_zip_code' => $request->customer_zip_code,
			]);

			$transaction->save();

			$total = 0;
			$totalShippingPrice = 0;
			foreach ($carts as $key => $cartItems) {
				# code...
				$transactionDetail = new TransactionDetail();
				$subtotal = 0;
				$shipping_price = $request->shipping_type == 'COP' ? 0 : $request->shipping_price;
				$transactionDetail->fill([
					'store_id' => $key,
					'transaction_id' => $transaction->id,
					'transaction_number' => strtoupper(generateRandomString(7)),
					'shipping_type' => $request->shipping_type,
					'shipping_price' => $shipping_price,
					'status' => 'NEW',
				]);
				$transactionDetail->save();

				foreach ($cartItems as $key => $item) {
					# code...
					$subtotal = $subtotal + $item->pivot->quantity * $item->price;
					$transactionDetail->items()->attach($item->id, [
						'transaction_id' => $transaction->id,
						'quantity' => $item->pivot->quantity,
						'price' => $item->price,
						'total_amount' => $item->pivot->quantity * $item->price,
						'notes' => $item->pivot->notes,
					]);

					//kurangi stok pada data product
					$item->stock = $item->stock - $item->pivot->quantity;
					$item->save();

					//hapus data cart
					$auth->carts()->detach($item->id);
				}

				$transactionDetail->subtotal = $subtotal;
				$transactionDetail->total_amount = $shipping_price + $subtotal;
				$transactionDetail->save();

				$total += $subtotal;
				$totalShippingPrice += $shipping_price;
			}

			$transaction->subtotal = $total;
			$transaction->shipping_price = $totalShippingPrice;
			$transaction->total_amount = $total + $shipping_price;
			$transaction->save();

			DB::commit();

			$response->code = 200;
			$response->results = $transaction;

			return response()->json($response, 200);
		}

		$response->code = 401;
		$response->message = 'Unauthorized';

		return response()->json(['error' => $response], 401);
	}

	public function payment(Request $request, $id)
	{
		$validator = Validator::make($request->all(), [
			'image' => 'required|image',
		]);

		$response = new stdClass();

		if ($validator->fails()) {
			$response->code = 203;
			$response->error = $validator->errors();
			return response()->json(['error' => $response], 203);
		}

		$transaction = Transaction::find($id);

		if (!$transaction) {
			$response->code = 204;
			$response->message = 'No data found';
			return response()->json(['error' => $response], 203);
		}

		$auth = Auth::user();
		if (isGuest($request) && $auth->email == $transaction->user->email) {
			DB::beginTransaction();
			$upload_path = config('storage.proof_of_payment');
			if (!File::exists($upload_path)) File::makeDirectory($upload_path, 0777, true);
			$file = $request->file('image');
			do {
				$image = generateRandomString(70) . '.' . $file->getClientOriginalExtension();
			} while (Transaction::where('proof_of_payment', '=', $image)->first() != null);

			if ($transaction->proof_of_payment) {
				File::delete($upload_path . '/' . $transaction->proof_of_payment);
			}

			$transaction->proof_of_payment = $image;
			$file->move($upload_path, $image);
			$transaction->save();

			$transaction->proof_of_payment = route(
				'media.proof_of_payment',
				['filename' => $transaction->proof_of_payment]
			);
			// foreach ($transaction->transactionDetails as $transactionDetail) {
			// 	$transactionDetail->status = 'NEW';
			// 	$transactionDetail->save();
			// }
			DB::commit();
			$response->code = 200;
			$response->message = 'Success';
			$response->results = $transaction;
			return response()->json($response, 200);
		}

		$response->code = 401;
		$response->message = 'Unauthorized';

		return response()->json(['error' => $response], 401);
	}
}
