<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use stdClass;
use Validator;

class CartController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function lists(Request $request)
    {
        $response = new stdClass();

        if (isGuest($request)) {
            $auth = Auth::user();
            $carts = $auth
                ->carts()
                ->select(
                    'products.id',
                    'products.name',
                    'products.price',
                    'products.store_id',
                    'products.stock'
                )
                ->get();

            foreach ($carts as $key => $value) {
                # code...
                $value->notes = $value->pivot->notes;
                $value->quantity = $value->pivot->quantity;
                $value->total = $value->quantity * $value->price;
                $value->store_name = $value->productStore->name;
                // set alert if product out of stock
                $value->alert = '';
                if ($value->quantity > $value->stock) {
                    $value->alert = 'Product out of stock';
                }

                $productImage = $value->productImages->first();
                if ($productImage)
                    $value->image = route('media.product', ['filename' => $productImage->image]);
                else
                    $value->image = url('/') . '/images/default.jpg';

                unset($value->pivot);
                unset($value->productImages);
                unset($value->productStore);
            }

            $response->code = 200;
            $response->results = $carts;

            return response()->json($response, 200);
        }

        $response->code = 401;
        $response->message = 'Unauthorized';

        return response()->json(['error' => $response], 401);
    }

    public function count(Request $request)
    {
        $response = new stdClass();

        if (isGuest($request)) {
            $auth = Auth::user();
            $count = $auth->carts()->count();

            $response->code = 200;
            $response->results = $count;

            return response()->json($response, 200);
        }

        $response->code = 401;
        $response->message = 'Unauthorized';

        return response()->json(['error' => $response], 401);
    }

    public function add(Request $request, $product_id)
    {
        $validator = Validator::make($request->all(), [
            'quantity' => 'required|numeric'
        ]);

        $response = new stdClass();

        if ($validator->fails()) {
            $response->code = 203;
            $response->error = $validator->errors();
            return response()->json(['error' => $response], 203);
        }

        $product = Product::find($product_id);

        if (!$product) {
            $response->code = 204;
            $response->message = 'No data found';
            return response()->json(['error' => $response], 200);
        }

        if (isGuest($request)) {
            $auth = Auth::user();
            $cart = $auth->carts()->find($product_id);
            if ($cart) {
                $quantity = $cart->pivot->quantity + $request->quantity;
                if ($product->stock < $quantity) {
                    $response->code = 203;
                    $response->message = 'Product out of stock';
                    return response()->json(['error' => $response], 203);
                }

                $auth
                    ->carts()
                    ->updateExistingPivot($product_id, ['quantity' => $quantity, 'notes' => $request->notes]);
            } else {
                if ($product->stock < $request->quantity) {
                    $response->code = 203;
                    $response->message = 'Product out of stock';
                    return response()->json(['error' => $response], 203);
                }
                $auth
                    ->carts()
                    ->attach($product_id, ['quantity' => $request->quantity, 'notes' => $request->notes]);
            }

            $response->code = 200;
            $response->message = 'success';

            return response()->json($response, 200);
        }

        $response->code = 401;
        $response->message = 'Unauthorized';

        return response()->json(['error' => $response], 401);
    }

    public function edit(Request $request, $product_id)
    {
        $validator = Validator::make($request->all(), [
            'quantity' => 'required|numeric'
        ]);

        $response = new stdClass();

        if ($validator->fails()) {
            $response->code = 203;
            $response->error = $validator->errors();
            return response()->json(['error' => $response], 203);
        }

        if (isGuest($request)) {
            $auth = Auth::user();
            $cart = $auth->carts()->find($product_id);
            if (!$cart) {
                $response->code = 204;
                $response->message = 'No data found';
                return response()->json(['error' => $response], 200);
            }

            $quantity = $request->quantity;

            if ($cart->stock < $quantity) {
                $response->code = 203;
                $response->message = 'Product out of stock';
                return response()->json(['error' => $response], 203);
            }

            $auth
                ->carts()
                ->updateExistingPivot($product_id, ['quantity' => $quantity, 'notes' => $request->notes]);

            $response->code = 200;
            $response->message = 'success';

            return response()->json($response, 200);
        }

        $response->code = 401;
        $response->message = 'Unauthorized';

        return response()->json(['error' => $response], 401);
    }

    public function delete(Request $request, $product_id)
    {
        $response = new stdClass();

        if (isGuest($request)) {
            $auth = Auth::user();
            $cart = $auth->carts()->find($product_id);
            if (!$cart) {
                $response->code = 204;
                $response->message = 'No data found';
                return response()->json(['error' => $response], 200);
            }

            $auth->carts()->detach($product_id);

            $response->code = 200;
            $response->message = 'success';

            return response()->json($response, 200);
        }

        $response->code = 401;
        $response->message = 'Unauthorized';

        return response()->json(['error' => $response], 401);
    }

    public function deleteAll(Request $request)
    {
        $response = new stdClass();

        if (isGuest($request)) {
            $auth = Auth::user();
            $count = $auth->carts()->detach();

            $response->code = 200;
            $response->results = $count;

            return response()->json($response, 200);
        }

        $response->code = 401;
        $response->message = 'Unauthorized';

        return response()->json(['error' => $response], 401);
    }
}
