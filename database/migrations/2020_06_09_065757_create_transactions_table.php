<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained('users');
            $table->string('invoice_number')->unique();
            $table->dateTime('date');
            $table->string('payment_type', 50)->nullable();
            $table->string('payment_status', 20);
            $table->string('source', 150);
            $table->unsignedInteger('discount_percent')->default(0);
            $table->decimal('discount_amount', 19, 2)->default(0);
            $table->decimal('subtotal', 19, 2)->default(0);
            $table->string('shipping_type', 30);
            $table->decimal('shipping_price', 19, 2)->default(0);
            $table->decimal('total_amount', 19, 2)->default(0);
            $table->string('customer_name', 150);
            $table->string('customer_email', 150);
            $table->string('customer_phone_number', 20);
            $table->text('customer_address');
            $table->foreignId('customer_province_id')->constrained('provinces');
            $table->foreignId('customer_city_id')->constrained('cities');
            $table->foreignId('customer_district_id')->constrained('districts');
            $table->foreignId('customer_village_id')->constrained('villages');
            $table->string('customer_zip_code', 30);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
