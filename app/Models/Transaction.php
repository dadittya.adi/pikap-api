<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
  protected $table = 'transactions';
  protected $fillable = [
    'user_id',
    'invoice_number',
    'date',
    'payment_type',
    'payment_status',
    'source',
    'discount_percent',
    'discount_amount',
    'subtotal',
    'shipping_type',
    'shipping_price',
    'total_amount',
    'customer_name',
    'customer_email',
    'customer_phone_number',
    'customer_address',
    'customer_province_id',
    'customer_city_id',
    'customer_district_id',
    'customer_village_id',
    'customer_zip_code',
    'proof_of_payment',
  ];

  protected $appends = ['proof_of_payment_url'];

  public function user()
  {
    return $this->belongsTo('App\Models\User', 'user_id');
  }

  public function transactionDetails()
  {
    return $this->hasMany('App\Models\TransactionDetail', 'transaction_id');
  }

  public function items()
  {
    return $this
      ->belongsToMany('App\Models\Product', 'transaction_items', 'transaction_id', 'product_id')
      ->withPivot(['quantity', 'price', 'total_amount', 'notes']);
  }

  public function getProofOfPaymentUrlAttribute()
  {
    $res = '';
    if ($this->proof_of_payment) {
      $res = route('media.proof_of_payment', ['filename' => $this->proof_of_payment]);
    }

    return $res;
  }
}
