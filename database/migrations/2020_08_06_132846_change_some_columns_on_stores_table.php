<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeSomeColumnsOnStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('stores', function (Blueprint $table) {
            //
            $table->string('email', 191)->nullable()->change();
            $table->string('phone_number', 20)->nullable()->change();

            $table->foreignId('area_id')->nullable()->constrained('cities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stores', function (Blueprint $table) {
            //
            $table->string('email', 191)->change();
            $table->string('phone_number', 20)->change();

            $table->dropForeign('stores_area_id_foreign');
            $table->dropColumn('area_id');
        });
    }
}
