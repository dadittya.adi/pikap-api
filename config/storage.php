<?php

return [
    'user_avatar' => storage_path() . '/app/user_avatar',
    'store_avatar' => storage_path() . '/app/store_avatar',
    'banner' => storage_path() . '/app/banner',
    'product_image' => storage_path() . '/app/product_image',
    'proof_of_payment' => storage_path() . '/app/proof_of_payment',
];
